//
//  FilterGeneralTableViewCell.swift
//  First Class And More
//
//  Created by Mikle Kusmenko on 10/7/17.
//  Copyright © 2017 Shawn Frank. All rights reserved.
//

import UIKit

class FilterGeneralTableViewCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var filterSwitch: SevenSwitch!
    
    var filterSwitchTapped: ((UITableViewCell, Bool) -> Void)?
    
    @IBAction func filterSwitchValueChanged(_ sender: SevenSwitch) {
        filterSwitchTapped?(self, sender.on)
    }
}
