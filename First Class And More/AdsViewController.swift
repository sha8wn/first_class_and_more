//
//  AdsViewController.swift
//  First Class And More
//

import UIKit

class AdsViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    var imageView = UIImageView()
    var imageName: String!
    var ad: AdvertisementModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
    }
    
    func setupData() {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentsURL.appendingPathComponent("ads/\(imageName!)")
        imageView.image = UIImage(contentsOfFile: fileURL.path)
        imageView.contentMode = .scaleAspectFill
        imageView.frame = UIScreen.main.bounds
        imageView.isUserInteractionEnabled = true
        scrollView.addSubview(imageView)
        scrollView.contentSize = imageView.image!.size
        scrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        imageView.addGestureRecognizer(tapGesture)
        
        let scrollViewFrame = scrollView.frame
        let scaleWidth = scrollViewFrame.size.width / scrollView.contentSize.width
        let scaleHeight = scrollViewFrame.size.height / scrollView.contentSize.height
        let minScale = min(scaleWidth, scaleHeight)
        scrollView.minimumZoomScale = minScale
        scrollView.maximumZoomScale = 5.0
        scrollView.zoomScale = 1.0
        scrollViewDidZoom(scrollView)
    }
    
    @objc func imageTapped() {
        print(Date())
        if let ad = ad, let _ = URL(string: ad.url), let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            dismiss(animated: true) {
                appDelegate.showWebView(ad.url)
            }
        }
    }
    
    @IBAction func closeBtnPressed() {
        print(Date())
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.timerFrequency = Double(appDelegate.timerSettings.frequency)
            appDelegate.restartTimer()
        }
        dismiss(animated: true, completion: nil)
    }
}

extension AdsViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let imageViewSize = imageView.frame.size
        let scrollViewSize = scrollView.bounds.size
        let verticalPadding = imageViewSize.height < scrollViewSize.height ? (scrollViewSize.height - imageViewSize.height) / 2 : 0
        let horizontalPadding = imageViewSize.width < scrollViewSize.width ? (scrollViewSize.width - imageViewSize.width) / 2 : 0
        scrollView.contentInset = UIEdgeInsets(top: verticalPadding, left: horizontalPadding, bottom: verticalPadding, right: horizontalPadding)
    }
}
