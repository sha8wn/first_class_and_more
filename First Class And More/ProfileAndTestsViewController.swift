//
//  ProfileAndTestsViewController.swift
//  First Class And More
//

import UIKit

class ProfileAndTestsViewController: SFSidebarViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var introLabel: UILabel!
    
    var categories: [Int] = []
    var profileAndTest: ProfileAndTest? {
        didSet {
           setupUI()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    func setupUI() {
        if let profileAndTest = profileAndTest {
            titleLabel?.text = profileAndTest.title?.html2String
            introLabel?.text = profileAndTest.intro?.html2String
        }
    }
    
    @IBAction func continueBtnPressed() {
        performSegue(withIdentifier: "showProfilesAndTestsCategoriesVC", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            if identifier == "showProfilesAndTestsCategoriesVC" {
                let dvc = segue.destination as! ProfileAndTestsCategoriesViewController
                dvc.dealsLoaded = false
                dvc.categories = categories
            }
        }
    }
}
