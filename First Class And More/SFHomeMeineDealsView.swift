//
//  SFMeineDealsView.swift
//  First Class And More
//
//  Created by Shawn Frank on 2/22/17.
//  Copyright © 2017 Shawn Frank. All rights reserved.
//

import UIKit

class SFHomeMeineDealsButton: UIButton
{
    var buttonDealType: DealType?
}

protocol SFHomeMeineDealsViewDelegate: class
{
    func meineDealButtonTapped(withType type: DealType)
}

class SFHomeMeineDealsView: UIView
{
    var paddingRatio = 1
    var delegate: SFHomeMeineDealsViewDelegate?
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
    }
    
    public func setUpButtons()
    {
        let buttonWidth = UIImage(named: "Deals0")?.size.width
        let buttonHeight = UIImage(named: "Deals0")?.size.height
        var y: CGFloat = 30.0
        var currentDeal = 0
        
        for i in 0...2
        {
            var x = (frame.size.width - ((buttonWidth! * 3) + (30 * 2))) / 2
            
            for j in 0...2
            {
                let dealButton = SFHomeMeineDealsButton(frame: CGRect(x: x, y: y, width: buttonWidth!, height: buttonHeight!))
                dealButton.setImage(UIImage(named: "Deals\(currentDeal)"), for: .normal)
                dealButton.addTarget(self, action: #selector(dealButtonTapped(button:)), for: .touchUpInside)
                dealButton.tag = i * 3 + j
                dealButton.buttonDealType = DealType(rawValue: currentDeal)
                addSubview(dealButton)
                
                x += buttonWidth! + 30
                currentDeal += 1
            }
            
            y += buttonHeight! + 30
        }
    }
    
    public func updateButtons(with color: DealState) {
        for subview in subviews {
            if let subview = subview as? SFHomeMeineDealsButton {
                let suffix = color == .gold ? "-gold" : ""
                let tag = subview.tag
                let image = UIImage(named: "Deals\(tag)\(suffix)")
                subview.setImage(image, for: .normal)
            }
        }
    }
    
    @objc public func dealButtonTapped(button: SFHomeMeineDealsButton)
    {
        self.delegate?.meineDealButtonTapped(withType: button.buttonDealType!)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }

}
