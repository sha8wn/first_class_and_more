//
//  RegisterViewController.swift
//  First Class And More
//
//  Created by Mikhail Kuzmenko on 9/24/18.
//  Copyright © 2018 Shawn Frank. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var newUserView: UIView!

    @IBOutlet weak var manRadioBtn: DLRadioButton!

    @IBOutlet weak var womenRadioBtn: DLRadioButton!

    @IBOutlet weak var surnameTextField: UITextField!

    @IBOutlet weak var emailTextField: UITextField!

    @IBOutlet weak var rulesCheckboxBtn: UIButton!

    @IBOutlet weak var newsLetterYesBtn: UIButton!

    @IBOutlet weak var newsLetterNoBtn: UIButton!

    @IBOutlet weak var newsLetterUserView: UIView!

    @IBOutlet weak var newsLetterEmailTextField: UITextField!

    @IBOutlet weak var newsLetterRulesCheckboxBtn: UIButton!

    @IBOutlet weak var premiumUserView: UIView!

    @IBOutlet weak var premiumEmailTextField: UITextField!

    @IBOutlet weak var premiumPasswordTextField: UITextField!

    @IBOutlet weak var premiumRulesCheckboxBtn: UIButton!

    var type: RegisterType?

    var state: Int?
    var wantSubscribe: Bool = true
    var rulesAccepted: Bool = false

    var newsLetterRulesAccepted: Bool = false

    var premiumRulesAccepted: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }

    private func setupView() {
        guard let type = type else { return }
        switch type {
        case .new:
            newUserView.isHidden = false
        case .newsletter:
            newsLetterUserView.isHidden = false
        case .premium:
            premiumUserView.isHidden = false
        }

        rulesCheckboxBtn.titleLabel?.numberOfLines = 2
        rulesCheckboxBtn.titleLabel?.textAlignment = .left
        newsLetterYesBtn.backgroundColor = #colorLiteral(red: 0.9313626885, green: 0.6842990518, blue: 0.1191969439, alpha: 1)
        newsLetterYesBtn.layer.cornerRadius = 5.0
        newsLetterYesBtn.layer.borderColor = #colorLiteral(red: 0.9294117647, green: 0.6823529412, blue: 0.1176470588, alpha: 1).cgColor
        newsLetterYesBtn.layer.borderWidth = 1.0
        newsLetterNoBtn.layer.cornerRadius = 5.0
        newsLetterNoBtn.layer.borderColor = #colorLiteral(red: 0.9313626885, green: 0.6842990518, blue: 0.1191969439, alpha: 1).cgColor
        newsLetterNoBtn.layer.borderWidth = 1.0

        newsLetterRulesCheckboxBtn.titleLabel?.numberOfLines = 2
        newsLetterRulesCheckboxBtn.titleLabel?.textAlignment = .left

        premiumRulesCheckboxBtn.titleLabel?.numberOfLines = 2
        premiumRulesCheckboxBtn.titleLabel?.textAlignment = .left
    }

    @IBAction func manRadioBtnPressed() {
        state = 1
        womenRadioBtn.isSelected = false
    }

    @IBAction func womenRadioBtnPressed() {
        state = 2
        manRadioBtn.isSelected = false
    }

    @IBAction func rulesCheckboxBtnPressed() {
        rulesAccepted = !rulesAccepted
        rulesCheckboxBtn.setImage(rulesAccepted ? #imageLiteral(resourceName: "chb-on") : #imageLiteral(resourceName: "chb-off"), for: .normal)
    }

    @IBAction func newsLetterRulesCheckboxBtnPressed() {
        newsLetterRulesAccepted = !newsLetterRulesAccepted
        newsLetterRulesCheckboxBtn.setImage(newsLetterRulesAccepted ? #imageLiteral(resourceName: "chb-on") : #imageLiteral(resourceName: "chb-off"), for: .normal)
    }

    @IBAction func premiumCheckboxBtnPressed() {
        premiumRulesAccepted = !premiumRulesAccepted
        premiumRulesCheckboxBtn.setImage(premiumRulesAccepted ? #imageLiteral(resourceName: "chb-on") : #imageLiteral(resourceName: "chb-off"), for: .normal)
    }

    @IBAction func newsLetterYesBtnPressed() {
        wantSubscribe = true
        newsLetterYesBtn.backgroundColor = #colorLiteral(red: 0.9313626885, green: 0.6842990518, blue: 0.1191969439, alpha: 1)
        newsLetterNoBtn.backgroundColor = .clear
    }

    @IBAction func newsLetterNoBtnPressed() {
        wantSubscribe = false
        newsLetterNoBtn.backgroundColor = #colorLiteral(red: 0.9313626885, green: 0.6842990518, blue: 0.1191969439, alpha: 1)
        newsLetterYesBtn.backgroundColor = .clear
    }

    @IBAction func registerBtnPressed() {
        guard let state = state else {
            showPopupDialog(title: "Ein Fehler ist aufgetreten..", message: "Wähle dein Geschlecht", cancelBtn: false)
            return
        }
        guard let email = emailTextField.text, let surname = surnameTextField.text else { return }
        if surname.isEmpty || email.isEmpty {
            let error = surname.isEmpty ? "Nachname fehlt!" : "E-Mail fehlt!"
            showPopupDialog(title: "Ein Fehler ist aufgetreten..", message: error, cancelBtn: false)
            return
        }
        if !rulesAccepted {
            showPopupDialog(title: "Ein Fehler ist aufgetreten..", message: "Bestätigen Sie Ihr Alter", cancelBtn: false)
            return
        }
        if isConnectedToNetwork(repeatedFunction: registerBtnPressed) {
            startLoading(message: "Wird geladen..")
            Server.shared.register(state: state, email: email, surname: surname, wantSubscribe: wantSubscribe) { success, error in
                DispatchQueue.main.async {
                    self.stopLoading()
                    if error != nil {
                        self.showPopupDialog(title: "Ein Fehler ist aufgetreten..", message: error!.description, cancelBtn: false)
                    } else {
                        if let success = success as? Bool, success {
                            self.performSegue(withIdentifier: "showHome", sender: nil)
                        }
                    }
                }
            }
        }
    }

    @IBAction func signInBtnPressed() {
        guard let email = newsLetterEmailTextField.text else { return }
        if email.isEmpty {
            showPopupDialog(title: "Ein Fehler ist aufgetreten..", message: "E-Mail fehlt!", cancelBtn: false)
            return
        }
        if !newsLetterRulesAccepted {
            showPopupDialog(title: "Ein Fehler ist aufgetreten..", message: "Bestätigen Sie Ihr Alter", cancelBtn: false)
            return
        }
        if isConnectedToNetwork(repeatedFunction: loginBtnPressed) {
            startLoading(message: "Wird geladen..")
            Server.shared.checkSubscriber(email: email) { isSubscribed, error in
                DispatchQueue.main.async {
                    self.stopLoading()
                    if error != nil {
                        self.showPopupDialog(title: "Ein Fehler ist aufgetreten..", message: error!.description, cancelBtn: false)
                    } else {
                        if let isSubscribed = isSubscribed as? Bool, isSubscribed {
                            self.performSegue(withIdentifier: "showHome", sender: nil)
                        }
                    }
                }
            }
        }
    }

    @IBAction func loginBtnPressed() {
        guard let email = premiumEmailTextField.text, let password = premiumPasswordTextField.text else { return }
        if email.isEmpty || password.isEmpty {
            let error = email.isEmpty ? "E-Mail fehlt!" : "Passwort fehlt!"
            showPopupDialog(title: "Ein Fehler ist aufgetreten..", message: error, cancelBtn: false)
            return
        }
        if !newsLetterRulesAccepted {
            showPopupDialog(title: "Ein Fehler ist aufgetreten..", message: "Bestätigen Sie Ihr Alter", cancelBtn: false)
            return
        }
        if isConnectedToNetwork(repeatedFunction: loginBtnPressed) {
            startLoading(message: "Wird geladen..")
            Server.shared.getPasswordSalt(email: email) { salt, error in
                DispatchQueue.main.async {
                    if error != nil {
                        self.stopLoading()
                        self.showPopupDialog(title: "Ein Fehler ist aufgetreten..", message: error!.description, cancelBtn: false)
                    } else {
                        if let salt = salt as? String {
                            self.performLogin(email: email, password: password, salt: salt)
                        }
                    }
                }
            }
        }
    }

    func performLogin(email: String, password: String, salt: String) {
        if isConnectedToNetwork(repeatedFunction: loginBtnPressed) {
            Server.shared.login(email: email, password: password, salt: salt) { success, error in
                DispatchQueue.main.async {
                    if error != nil {
                        self.stopLoading()
                        self.showPopupDialog(title: "Ein Fehler ist aufgetreten..", message: error!.description)
                    } else {
                        if let success = success as? Bool, success {
                            self.getUserInfo()
                            UIApplication.shared.registerForRemoteNotifications()
                        }
                    }
                }
            }
        }
    }

    func getUserInfo() {
        if isConnectedToNetwork(repeatedFunction: getUserInfo) {
            Server.shared.getSettings() { success, error in
                DispatchQueue.main.async {
                    self.stopLoading()
                    if error != nil {
                        self.showPopupDialog(title: "Ein Fehler ist aufgetreten..", message: error!.description)
                    } else {
                        if let success = success as? Bool, success {
                            UserDefaults.standard.set(true, forKey: kUDUserRegistered)
                            self.performSegue(withIdentifier: "showHome", sender: nil)
                        }
                    }
                }
            }
        }
    }

    @IBAction func closeBtnPressed() {
        dismiss(animated: true, completion: nil)
    }
}

extension RegisterViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}
