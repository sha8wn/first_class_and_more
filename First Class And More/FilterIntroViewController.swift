//
//  FilterIntroViewController.swift
//  First Class And More
//
//  Created by Mikle Kusmenko on 4/17/17.
//  Copyright © 2017 Shawn Frank. All rights reserved.
//

import UIKit

class FilterIntroViewController: SFSidebarViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupData()
    }
    
    func setupData() {
        if let data = UserDefaults.standard.object(forKey: kUDSettingsFilter) as? Data,
            let filter = NSKeyedUnarchiver.unarchiveObject(with: data) as? FilterObject {
            if let title = filter.title {
                titleLabel.text = title
            }
            if let body = filter.body {
                bodyLabel.text = body
            }
        }
    }
    
    @IBAction func continueBtnPressed() {
        performSegue(withIdentifier: "showFilterVC", sender: nil)
    }
}
