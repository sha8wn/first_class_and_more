//
//  SFHomeCarouselView.swift
//  First Class And More
//
//  Created by Shawn Frank on 2/22/17.
//  Copyright © 2017 Shawn Frank. All rights reserved.
//

import UIKit
import AlamofireImage
import DZNEmptyDataSet

protocol CarouselDelegate {
    func slideSelected(slide: SlideModel) -> Void
    func showPopup() -> Void
}

class SFHomeCarouselView: UIView, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    var featuredCarousel: UICollectionView!
    var delegate: CarouselDelegate?
    
    weak var carouselTimer: Timer?
    var currentPage         = 0
    let carouselWaitTime    = 6
    var currentCarouselTick = 0
    
    var slides: [SlideModel] = []
    
    public func configureCarousel()
    {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset            = .zero
        layout.itemSize                = bounds.size
        layout.minimumLineSpacing      = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection         = .horizontal
        featuredCarousel = UICollectionView(frame: bounds, collectionViewLayout: layout)
        featuredCarousel.register(UINib(nibName: "SliderCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SliderCollectionViewCell")
        featuredCarousel.backgroundColor                = .white
        featuredCarousel.delegate                       = self
        featuredCarousel.dataSource                     = self
        featuredCarousel.emptyDataSetSource             = self
        featuredCarousel.emptyDataSetDelegate           = self
        featuredCarousel.isPagingEnabled                = true
        featuredCarousel.alwaysBounceHorizontal         = true
        featuredCarousel.showsVerticalScrollIndicator   = false
        featuredCarousel.showsHorizontalScrollIndicator = false
        addSubview(featuredCarousel)
    }
    
    func updateCarousel(slides: [SlideModel]) {
        self.slides = slides
        featuredCarousel.reloadData()
        invalidateTimer()
    }

    private func invalidateTimer() {
        carouselTimer?.invalidate()
        carouselTimer = nil
        carouselTimer = Timer.scheduledTimer(
            timeInterval: TimeInterval(carouselWaitTime),
            target: self,
            selector: #selector(carouselTick),
            userInfo: nil,
            repeats: true
        )
    }
    
    // MARK: - Carousel Management
    
    @objc private func carouselTick()
    {
        currentCarouselTick += 1
        featuredCarousel.isUserInteractionEnabled = false
        if currentPage < slides.count - 1 {
            featuredCarousel.setContentOffset(CGPoint(x: featuredCarousel.contentOffset.x + featuredCarousel.frame.size.width, y: featuredCarousel.contentOffset.y), animated: true)
        } else if currentPage == slides.count - 1 {
            featuredCarousel.setContentOffset(.zero, animated: true)
        }
    }
    
    public func configureCarouselStatus()
    {
        currentPage = Int(featuredCarousel.contentOffset.x / featuredCarousel.frame.size.width)
        currentCarouselTick = 0
        featuredCarousel.isUserInteractionEnabled = true
    }
    
    // MARK: CollectionView Delegate & DataSource
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        configureCarouselStatus()
        invalidateTimer()
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        configureCarouselStatus()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return slides.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCollectionViewCell", for: indexPath) as! SliderCollectionViewCell
        configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: SliderCollectionViewCell, atIndexPath indexPath: IndexPath) {
        let index = indexPath.row
        let slide = slides[index]
        cell.slideTitleLabel.text = slide.title
        cell.slideShortTitleButton.setTitle(slide.shortTitle?.uppercased(), for: .normal)
        cell.slideImageView.image = nil
        if let urlString = slide.imageUrl, let url = URL(string: urlString) {
            cell.activityIndicator.startAnimating()
            cell.titleView.isHidden = true
            cell.slideImageView.af_setImage(
                withURL: url,
                progressQueue: DispatchQueue.main,
                imageTransition: .crossDissolve(0.2),
                runImageTransitionIfCached: false,
                completion: { image in
                    cell.activityIndicator.stopAnimating()
                    cell.titleView.isHidden = false
                }
            )
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let index = indexPath.row
        let slide = slides[index]
        if let access = slide.access, access == 1 {
            delegate?.slideSelected(slide: slide)
        } else {
            delegate?.showPopup()
        }
    }
    
    // MARK: Empty dataset
    
//    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
//        return #imageLiteral(resourceName: "empty")
//    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let title = "Keine Folien"
        let attributes = [
            NSAttributedString.Key.font: UIFont(name: "RobotoCondensed-Regular", size: 24.0)!,
            NSAttributedString.Key.foregroundColor: UIColor.black
        ]
        return NSAttributedString(string: title, attributes: attributes)
    }
    
//    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
//        let title = "Some very long test description about empty dataset"
//        let attributes = [
//            NSFontAttributeName: UIFont(name: "RobotoCondensed-Regular", size: 17.0)!,
//            NSForegroundColorAttributeName: UIColor.darkGray
//        ]
//        return NSAttributedString(string: title, attributes: attributes)
//    }
}
