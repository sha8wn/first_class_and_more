//
//  WelcomeViewController.swift
//  First Class And More
//
//  Created by Mikhail Kuzmenko on 9/24/18.
//  Copyright © 2018 Shawn Frank. All rights reserved.
//

import UIKit
import QuickLook

enum RegisterType {
    case new, newsletter, premium
}

class WelcomeViewController: UIViewController {

    @IBOutlet weak var newUserBtn: UIButton!

    @IBOutlet weak var newsletterUserBtn: UIButton!

    @IBOutlet weak var premiumUserBtn: UIButton!

    let fileNames = ["agb.docx", "datenschutz.docx"]

    var fileURLs = [URL]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        prepareFileURLs()
    }

    private func setupView() {
        setupButton(button: newUserBtn, string: "Ich bin neu bei First Class & More und\nhabe mich bisher noch nicht registriert", mediumPart: "neu bei First Class & More")
        setupButton(button: newsletterUserBtn, string: "Ich bin bereits First Class & More\nNewsletter-Empfänger", mediumPart: "Newsletter-Empfänger")
        setupButton(button: premiumUserBtn, string: "Ich bin Premium-Mitglied bei\nFirst Class & More", mediumPart: "Premium-Mitglied")
    }

    private func setupButton(button: UIButton, string: String, mediumPart: String) {
        let attributedTitle = NSMutableAttributedString(string: string)
        attributedTitle.addAttributes([
            .font: UIFont(name: "Roboto-Light", size: 14.0)!,
            .foregroundColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        ], range: NSRange(location: 0, length: attributedTitle.length))
        attributedTitle.addAttributes([
            .font: UIFont(name: "Roboto-Medium", size: 14.0)!,
            .foregroundColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        ], range: (attributedTitle.string as NSString).range(of: mediumPart))
        button.setAttributedTitle(attributedTitle, for: .normal)
        button.titleLabel?.numberOfLines = 2
        button.titleLabel?.textAlignment = .left
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.minimumScaleFactor = 0.7
    }

    func prepareFileURLs() {
        for file in fileNames {
            let fileParts = file.components(separatedBy: ".")
            if let fileUrl = Bundle.main.url(forResource: fileParts[0], withExtension: fileParts[1]) {
                if FileManager.default.fileExists(atPath: fileUrl.path)  {
                    fileURLs.append(fileUrl)
                }
            }
        }
    }

    @IBAction func newUserBtnPressed() {
        performSegue(withIdentifier: "showRegister", sender: RegisterType.new)
    }

    @IBAction func newsLetterUserBtnPressed() {
        performSegue(withIdentifier: "showRegister", sender: RegisterType.newsletter)
    }

    @IBAction func premiumUserBtnPressed() {
        performSegue(withIdentifier: "showRegister", sender: RegisterType.premium)
    }

    @IBAction func agbBtnPressed() {
        openFile(index: 0)
    }

    @IBAction func datenschutzBtnPressed() {
        openFile(index: 1)
    }

    func openFile(index: Int) {
        let quickLookController = QLPreviewController()
        quickLookController.dataSource = self
        quickLookController.currentPreviewItemIndex = index
        present(quickLookController, animated: true, completion: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        switch identifier {
        case "showRegister":
            let dvc = segue.destination as? RegisterViewController
            dvc?.type = sender as? RegisterType
        default:
            break
        }
    }
}

extension WelcomeViewController: QLPreviewControllerDataSource {
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }

    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return fileURLs[index] as QLPreviewItem
    }
}
