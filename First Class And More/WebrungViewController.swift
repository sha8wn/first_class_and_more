//
//  WebrungViewController.swift
//  First Class And More
//
//  Created by Mikle Kusmenko on 8/6/17.
//  Copyright © 2017 Shawn Frank. All rights reserved.
//

import UIKit

class WebrungViewController: SFSidebarViewController {

    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var shortTermRadioBtn: DLRadioButton!
    @IBOutlet weak var regularRadioBtn: DLRadioButton!
    
    var shortTermSelected: Bool = false
    var regularSelected: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        addHomeBtn()
        createTopTitle()
        if let dict = UserDefaults.standard.value(forKey: kUDAdsSettings) as? [String: Bool] {
            if let short = dict["shortTermSelected"] {
                shortTermSelected = short
                shortTermRadioBtn.isSelected = short
                if short { redrawButtons(shortTermRadioBtn, selected: short) }
            }
            if let regular = dict["regularSelected"] {
                regularSelected = regular
                regularRadioBtn.isSelected = regular
                if regular { redrawButtons(regularRadioBtn, selected: regular) }
            }
        }
    }
    
    func createTopTitle() {
        let titleLabel       = SFFCAMLabel()
        titleLabel.type      = .Heading
        titleLabel.textColor = fcamBlue
        titleLabel.text      = "Promotions"
        titleLabel.sizeToFit()
        titleLabel.frame.origin.x = (UIScreen.main.bounds.width - titleLabel.frame.size.width) / 2
        titleLabel.frame.origin.y = (titleView.frame.size.height - titleLabel.frame.size.height) / 2
        titleView.addSubview(titleLabel)
    }
    
    @IBAction func shortTermBtnPressed(_ sender: DLRadioButton) {
        shortTermSelected = !shortTermSelected
        shortTermRadioBtn.isSelected = shortTermSelected
        redrawButtons(sender, selected: shortTermSelected)
        sendAndSave()
    }
    
    @IBAction func regularBtnPressed(_ sender: DLRadioButton) {
        regularSelected = !regularSelected
        regularRadioBtn.isSelected = regularSelected
        redrawButtons(sender, selected: regularSelected)
        sendAndSave()
    }
    
    private func sendAndSave() {
        let dict: [String: Bool] = ["shortTermSelected": shortTermSelected, "regularSelected": regularSelected]
        UserDefaults.standard.set(dict, forKey: kUDAdsSettings)
        UserDefaults.standard.synchronize()
        var value: Int
        switch (shortTermSelected, regularSelected) {
            case (true, false):
                value = 2
            case (false, true):
                value = 1
            case (true, true):
                value = 3
            case (false, false):
                value = 4
        }
        updateAdsSettings(value)
    }
    
    func redrawButtons(_ btn: DLRadioButton, selected: Bool) {
        btn.setTitleColor(selected ? fcamGold : fcamBlue, for: .normal)
        btn.iconColor = selected ? fcamGold : fcamBlue
    }
    
    private func updateAdsSettings(_ ads: Int) {
        if isConnectedToNetwork(repeatedFunction: {
            self.updateAdsSettings(ads)
        }) {
            startLoading()
            Server.shared.changeAdsSettings(ads) { response, error in
                DispatchQueue.main.async {
                    self.stopLoading()
                    if error != nil {
                        self.showPopupDialog(title: "Ein Fehler ist aufgetreten..", message: error!.description)
                    }
                }
            }
        }
    }
}
